# singularity-jupyter

Jupyterlab notebook servier in Singularity with Holoviews, Altair, PyViz,
Matplotlib, PyLantern, etc.


## how it works

We assume that the singularity image is here
`````
     /srv/singularity-images
`````

and that there are a couple bash shell scripts and a jupyter config file template  
that are also in that directory:
`````
     jupyter_notebook_config.py
     findport-jupyterlab
     run-jupyterlab
````` 
The run-jupyterlab script copies the jupyter config file template into the user  
home directory tree and also copies the findport-jupyterlab script into the user's homedir.  
Singularity overlays the host's unix filesystem except (by default) for the user homedir  
and /tmp, and we assume we are running from the user's homedir.

After putting the files in place, run-jupyterlab tells singularity to exec findport-jupyterlab.

Findport-jupyterlab locates an unused port so tere will not be port collision for web access,  
tells the user how to connect via ssh port forwarding to the singularity container, generates a  
token to be used to authenticate to jupyterlab and launches jupyterlab within the container.  




